package javaQ2;

import java.util.LinkedList;

public class Linkedlist {
	public static void main(String[] args) {
		LinkedList<String>linkedlist=new LinkedList<>();
		linkedlist.add("7up");
		linkedlist.add("pepsi");
		linkedlist.add("bovanta");
		linkedlist.add("soda");
		linkedlist.add("applejuice");
		System.out.println(linkedlist.size());
		System.out.println(linkedlist);
		linkedlist.remove(1);
		System.out.println("after removing second item:"+linkedlist);
		boolean contains = linkedlist.contains("pepsi");
		System.out.println("list contain pepsi:" +contains);
}
}
