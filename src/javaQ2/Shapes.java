package javaQ2;

public abstract class Shapes {
	
	abstract void rectangleArea(int length , int breadth);
    abstract void squareArea(int side);
    abstract void circleArea(float radius);


}
