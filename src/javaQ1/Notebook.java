package javaQ1;

public class Notebook extends Book {
	public void draw()
	{
		System.out.println("this picture have drawn by me");
	}
	
	@Override
	public void write() {
		System.out.println("this sentence is good");
		
	}
	@Override
	public void read() {
		System.out.println("Book is ready to publish");
		
	}
	public static void main(String[] args) {
		Notebook Note=new Notebook();
		Note.write();
		Note.read();
		Note.draw();
		
	}
}