package javaQ1;

public class Polymorphism {

	public static void main(String[] args) {
		Car car1 = new Car();
		Car car2 = new Santro();

		car1.drive();
		car1.stop();

		car2.drive();
		car2.stop();

	}
}

	